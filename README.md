# README #

Exercice avec Symfony 5.2 et l'API pole emploi

### Prérequis ###

Installer Docker et Docker compose sur votre environnement

* https://hub.docker.com/search?type=edition&offering=community
* https://docs.docker.com/compose/install/

### Démarrer l'API ###

* Faire un docker-compose build
* Faire docker-compose up -d
* Installer les vendor si les fichiers ne sont pas présents : docker exec apidisplay_php_1 composer install

* Le site est disponible en localhost:8080