<?php

declare(strict_types=1);

namespace App\Controller;

use App\Integrations\PoleEmploi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    public function showOffers(PoleEmploi $poleEmploiApi): Response
    {
        $offers = $poleEmploiApi->getOffers();

        if (!empty($offers['error'])) {
            $this->addFlash('error', $offers['error']);
            $offers = [];
        }

        return $this->render('offers/list.html.twig', ['offers' => $offers]);
    }
}
