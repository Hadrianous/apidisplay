<?php

declare(strict_types=1);

namespace App\Integrations\DTO\PoleEmploi\Serializer;

use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class FrenchConverter implements NameConverterInterface
{
    private const PROPERTY_MAPPING = [
        'intitule' => 'title',
        'typeContrat' => 'contractType',
        'typeContratLibelle' => 'contractTypeTitle',
        'lieuTravail' => 'workplace',
        'entreprise' => 'company',
        'libelle' => 'label',
        'nom' => 'name',
    ];

    public function normalize(string $propertyName)
    {
        $mapping = array_flip(self::PROPERTY_MAPPING);
        return $mapping[$propertyName] ?? $propertyName;
    }

    public function denormalize(string $propertyName)
    {
        return self::PROPERTY_MAPPING[$propertyName] ?? $propertyName;
    }
}
