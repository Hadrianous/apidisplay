<?php

declare(strict_types=1);

namespace App\Integrations\DTO\PoleEmploi;

class Offer
{
    private string $title = '';
    private string $description = '';
    private string $contractTypeTitle = '';
    private string $contractType = '';
    private Workplace $workplace;
    private Company $company;


    public function getDescription(): string
    {
        return $this->description;
    }

    public function setWorkplace(Workplace $workplace): self
    {
        $this->workplace = $workplace;
        return $this;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;
        return $this;
    }

    public function getWorkplace(): Workplace
    {
        return $this->workplace;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getContractTypeTitle(): string
    {
        return $this->contractTypeTitle;
    }

    public function setContractTypeTitle(string $contractTypeTitle): void
    {
        $this->contractTypeTitle = $contractTypeTitle;
    }

    public function setContractType(string $contractType): void
    {
        $this->contractType = $contractType;
    }

    public function getContractType(): string
    {
        return $this->contractType;
    }
}
