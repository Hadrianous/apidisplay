<?php

declare(strict_types=1);

namespace App\Integrations\DTO\PoleEmploi;

class Company
{
    private string $name = '';

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
