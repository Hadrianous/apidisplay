<?php

declare(strict_types=1);

namespace App\Integrations\DTO\PoleEmploi;

class Workplace
{
    private string $label = '';

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }
}
