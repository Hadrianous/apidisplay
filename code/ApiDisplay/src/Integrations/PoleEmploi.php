<?php

declare(strict_types=1);

namespace App\Integrations;

use App\Integrations\DTO\PoleEmploi\Offer;
use App\Integrations\DTO\PoleEmploi\Serializer\FrenchConverter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\JsonException;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PoleEmploi
{
    private HttpClientInterface $client;

    private array $params;

    private Serializer $serializer;

    public function __construct(HttpClientInterface $client, ParameterBagInterface $params)
    {
        $this->client = $client;
        $this->params = $params->get('integrations')['pole_emploi'];

        $nameConverter = new FrenchConverter();
        $normalizer = new ObjectNormalizer(null, $nameConverter, null, new ReflectionExtractor());
        $this->serializer = new Serializer([$normalizer]);
    }

    private function authenticate(): ?string
    {
        $response = $this->client->request(
            'POST',
            $this->params['api']['connection.access_token'] . '?realm=partenaire',
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'body' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $this->params['credentials']['key'],
                    'client_secret' => $this->params['credentials']['secret'],
                    'scope' => "application_{$this->params['credentials']['key']} api_offresdemploiv2 o2dsoffre",
                ]
            ]
        );

        try {
            $data = $response->toArray();
        } catch (ClientException $e) {
            // Todo log error
            return null;
        }

        return $data['access_token'] ?? null;
    }
    
    public function getOffers(int $range = 0, int $limit = 9, int $codeVille = 33063, int $sort = 0): array
    {
        // TODO cache token in redis
        $token = $this->authenticate();

        $response = $this->client->request(
            'GET',
            $this->params['api']['offers.search'] . "?range={$range}-{$limit}&commune=$codeVille&sort=$sort",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]
        );

        try {
            $data = $response->toArray();
            $results = !empty($data) ? $data['resultats'] : [];

            $offers = [];
            foreach ($results as $offer) {
                $offers[] = $this->serializer->denormalize(
                    $offer,
                    Offer::class
                );
            }

            return $offers;
        } catch (ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | NotNormalizableValueException $e) {
            return ['error' => 'Erreur ' . $e->getMessage()];
        } catch (JsonException $e) { // When response body is empty
            return [];
        }
    }
}
